from src.entities.customer import Customer
from src.services.authenticationservice import AuthenticationService

auth = AuthenticationService()

def test_authetication_when_cust_is_null():
    #Given
    cust: Customer = None
    expected = False
    
    #When
    result = auth.login(cust)
    
    #Then

    assert result == expected



def test_authetication_when_cust_is_not_null_and_success():
    # Given
    name = "nassim"
    pwd = "1"
    cust = Customer(name,pwd)
    expected = True

    # When
    result = auth.login(cust)

    # Then

    assert result == expected

def test_authetication_when_cust_is_not_null_and_fail():
    # Given
    name = "nassim"
    pwd =""
    cust = Customer(name,pwd)
    expected = False

    # When
    result = auth.login(cust)

    # Then

    assert result == expected