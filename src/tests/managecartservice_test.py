def add_product_to_cart_when_product_is_null():
    # Given
    product = None
    cart = Cart()
    expected = cart.get_products()

    # When
    manager.add_product_to_cart(cart, product)
    result = cart.get_products()

    # Then
    assert result == expected


def add_product_to_cart_when_product_is_not_null():
    # Given
    product = None
    cart = Cart()
    expected = cart.get_products()

    # When
    manager.add_product_to_cart(cart, product)
    result = cart.get_products()

    # Then
    assert result == expected

