class Cart:

    def __init__(self):
        self.products = []

    def get_products(self):
        return self.products

    def add_product(self,product):
        self.products.append(product)

    def expose_cart(self):
        if (len(self.get_products())==0):
            print("your cart is empty!")
        else:
            print('Your cart contains :')
            for p in self.get_products():
                p.description()


