from src.entities.cart import Cart
from src.entities.product import Product


class Customer:

    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd
        self.cart = Cart()

    def get_name(self):
        return self.name

    def get_password(self):
        return self.pwd

    def display_cart(self):
        if (len(self.cart.products) == 0):
            print('Your cart is empty')
        for p in self.cart.products:
            p.display()

    def add_to_cart(self, product):
        self.cart.add_product(product)

    def select_product(self, products):
        return products[0]

    def validate_cart(self, choice):
        return (choice == "yes" and len(self.cart.products) > 0)

    def pay(self, amount, credit):
        return credit >= amount


"""    
def login(self, login, pass_word):
        return (login == 1 and pass_word == "1111")
"""
