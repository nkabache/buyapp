from src.services.authenticationservice import AuthenticationService
from src.entities.product import *
from src.entities.shop import *
from src.entities.customer import *

if __name__ == '__main__':

    p1 = Product(1, "category1")
    p2 = Product(2, "category3")
    p3 = Product(4, "category5")

    # 0- initialize Shop
    shop = Shop()
    shop.add_product(p1)
    shop.add_product(p2)
    shop.add_product(p3)

    print("Shop is Initiliazed with products")

    # 1- get Client
    cust: Customer = Customer("nassim", "1")

    # 2- Authenticate our client
    is_authenticted = AuthenticationService.login(AuthenticationService, cust)

    # 3- get list of products
    if (is_authenticted):
        print("Customer is Authenticated")
        cust.cart = Cart()
        cust.display_cart()

        print('Our products ')
        product_list = shop.expose_products()
        for p in product_list:
            p.display()

        # 4- Select a product
        print('product selected')
        selected_product = cust.select_product(product_list)

        # 5- add the product to Cart
        cust.add_to_cart(selected_product)

        cust.display_cart()

        # 6- validate the cart
        is_validated = cust.validate_cart("yes")

        if (is_validated):
            print('Your cart is validated')

            # 7- payment
            is_payed = cust.pay(10, 13)
            if (is_payed):
                print('your payment is valid')
            else:
                print('your payment is  not validated')
        else:
            print('Your cart is not validated')
    else:
        print("Authentication failed")
